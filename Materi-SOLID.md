**SOLID** adalah singkatan dari lima prinsip desain perangkat lunak yang dikembangkan untuk membantu menghasilkan kode yang mudah dipahami, mudah dikelola, dan dapat dengan mudah diperluas. 

**S** ingle Responsibility Principle,
 
**O** pen/Closed Principle,
 
**L** iskov Substitution Principle,
 
**I** nterface Segregation Principle,
 
**D** ependency Inversion Principle. 
 
Berikut adalah rincian tentang masing-masing prinsip SOLID:

# 1. Single Responsibility Principle (Prinsip Tanggung Jawab Tunggal):

Prinsip ini menyatakan bahwa sebuah kelas atau modul seharusnya hanya memiliki satu tanggung jawab tunggal. Artinya, sebuah kelas atau modul harus bertanggung jawab  hanya satu aspek dari fungsionalitas perangkat lunak. 

- Contoh analogi yaitu  pemesanan makanan di restoran.


- Contoh Code

```
class Pelanggan {
    private String nama;
    
    // Konstruktor Pelanggan untuk menginisialisasi nama pelanggan
    public Pelanggan(String nama) {
        this.nama = nama;
    }
    
    // Metode untuk mendapatkan nama pelanggan
    public String getNama() {
        return nama;
    }
    
    // Metode untuk memberi sapaan kepada pelanggan
    public String sapa() {
        return "Halo, " + nama + "!";
    }
}

class Pesanan {
    private Pelanggan pelanggan;
    private String produk;
    
    // Konstruktor Pesanan untuk menginisialisasi pelanggan dan produk pesanan
    public Pesanan(Pelanggan pelanggan, String produk) {
        this.pelanggan = pelanggan;
        this.produk = produk;
    }
    
    // Metode untuk menghasilkan pesan checkout dengan menggabungkan nama pelanggan dan produk pesanan
    public String checkout() {
        return pelanggan.getNama() + " memesan " + produk + ".";
    }
}

public class Main {
    public static void main(String[] args) {
        // Membuat objek pelanggan dengan nama "El"
        Pelanggan pelanggan = new Pelanggan("El");
        
        // Membuat objek pesanan dengan menggunakan objek pelanggan dan produk "Smartphone"
        Pesanan pesanan = new Pesanan(pelanggan, "Smartphone");
        
        // Menampilkan sapaan kepada pelanggan
        System.out.println(pelanggan.sapa());
        
        // Menampilkan pesan checkout
        System.out.println(pesanan.checkout());
    }
}

```

- output
![Dokumentasi](Dokumentasi/S.png)

> **Penjelasan**
 

kode yang menerapkan prinsip Single Responsibility Principle (SRP):

Dengan menerapkan prinsip ini, kita memastikan bahwa perubahan dalam satu tanggung jawab tidak berdampak pada tanggung jawab lainnya.

- Kelas "Pelanggan" (Customer):Berfungsi untuk Mengelola data pelanggan dan mengakses informasi tentang pelanggan. Kode ini menerapkan SRP karena hanya bertanggung jawab terhadap data pelanggan.

```
class Pelanggan {
    private String nama;
    
    // Konstruktor Pelanggan untuk menginisialisasi nama pelanggan
    public Pelanggan(String nama) {
        this.nama = nama;
    }
    
    // Metode untuk mendapatkan nama pelanggan
    public String getNama() {
        return nama;
    }
    
    // Metode untuk memberi sapaan kepada pelanggan
    public String sapa() {
        return "Halo, " + nama + "!";
    }
}
```
- Kelas "Pesanan" (Order): Mengelola pesanan dan operasi checkout. Kode ini menerapkan SRP karena hanya bertanggung jawab terhadap representasi pesanan dan operasi checkout.

```
class Pesanan {
    private Pelanggan pelanggan;
    private String produk;
    
    // Konstruktor Pesanan untuk menginisialisasi pelanggan dan produk pesanan
    public Pesanan(Pelanggan pelanggan, String produk) {
        this.pelanggan = pelanggan;
        this.produk = produk;
    }
    
    // Metode untuk menghasilkan pesan checkout dengan menggabungkan nama pelanggan dan produk pesanan
    public String checkout() {
        return pelanggan.getNama() + " memesan " + produk + ".";
    }
}
```


- Kelas "Main": Menjalankan program utama. Meskipun tidak secara langsung terkait dengan SRP, kelas ini bertindak sebagai entry point untuk menjalankan logika utama.

```
public class Main {
    public static void main(String[] args) {
        // Membuat objek pelanggan dengan nama "El"
        Pelanggan pelanggan = new Pelanggan("El");
        
        // Membuat objek pesanan dengan menggunakan objek pelanggan dan produk "Smartphone"
        Pesanan pesanan = new Pesanan(pelanggan, "Smartphone");
        
        // Menampilkan sapaan kepada pelanggan
        System.out.println(pelanggan.sapa());
        
        // Menampilkan pesan checkout
        System.out.println(pesanan.checkout());
    }
}
```


Pada kelas "Pelanggan" dan "Pesanan", memisahkan tanggung jawabnya masing-masing sehingga setiap kelas memiliki satu alasan untuk berubah dan memiliki fokus yang jelas.

> **Penjelasan Dari Analogi Yang Dibuat**

1. Kelas Pelanggan dapat di analogikan sebagai seorang pelanggan yang datang ke sebuah restoran. 

2. pada kelas Pelanggan metode `getNama()` di analogikan sebagai cara untuk mendapatkan nama pelanggan.

3.  pada kelas Pelanggan metode `sapa()` di analogikan sebagai ucapan sapaan yang diberikan restoran kepada pelanggan dengan nama dari pelanggan. 

4. Kelas **Pesanan**  di analogikan sebagai pesanan makanan yang diberikan oleh pelanggan ke restoran terdiri dari pelanggan dan produk yang dipesan.

5.  pada kelas Pesanan metode `checkout()`  di analogikan sebagai langkah pembayaran dan pengecekan pesanan oleh restoran lalu Restoran menyebutkan nama pelanggan dan produk yang dipesan dalam proses pengecekan.

6. `Pada bagian main()`, objek pelanggan diinisialisasi dengan nama "El", menggambarkan seorang pelanggan bernama El yang datang ke restoran.

7. Objek pesanan diinisialisasi dengan pelanggan "El" dan produk "Smartphone", menggambarkan pesanan makanan dari pelanggan El.

8. Pada pemanggilan `metode sapa()`, restoran menyapa pelanggan dengan menyebutkan nama pelanggan.

9. Pada pemanggilan metode `checkout()`, restoran mengecek pesanan yang dilakukan oleh pelanggan El dan mencantumkan nama pelanggan dan produk yang dipesan.".

kita dapat melihat bagaimana kelas Pelanggan dan Pesanan bekerja dalam konteks proses pemesanan makanan di restoran.


# 2. Open/Closed Principle (Prinsip Terbuka/Tertutup):

Prinsip ini menyatakan bahwa entitas perangkat lunak seharusnya terbuka untuk perluasan (Terbuka untuk extensi), tetapi tertutup untuk modifikasi (tertutup untuk modifikasi). Dengan kata lain, ketika perluasan fungsionalitas diperlukan, kita harus dapat menambahkan kode baru tanpa mengubah kode yang sudah ada.

- Contoh analogi dalam bidang-bidang geometri.


- Contoh kode

```
interface Bentuk {
    double hitungLuas();
}

class PersegiPanjang implements Bentuk {
    private double panjang;
    private double lebar;
    
    // Konstruktor PersegiPanjang untuk menginisialisasi panjang dan lebar
    public PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }
    
    // Implementasi metode hitungLuas() untuk menghitung luas persegi panjang
    public double hitungLuas() {
        return panjang * lebar;
    }
}

class Lingkaran implements Bentuk {
    private double jariJari;
    
    // Konstruktor Lingkaran untuk menginisialisasi jari-jari
    public Lingkaran(double jariJari) {
        this.jariJari = jariJari;
    }
    
    // Implementasi metode hitungLuas() untuk menghitung luas lingkaran
    public double hitungLuas() {
        return Math.PI * jariJari * jariJari;
    }
}

public class Main {
    public static void main(String[] args) {
        // Membuat objek persegiPanjang dengan panjang 5 dan lebar 3
        Bentuk persegiPanjang = new PersegiPanjang(5, 3);
        
        // Membuat objek lingkaran dengan jari-jari 2.5
        Bentuk lingkaran = new Lingkaran(2.5);
        
        // Menampilkan luas persegi panjang
        System.out.println("Luas Persegi Panjang: " + persegiPanjang.hitungLuas());
        
        // Menampilkan luas lingkaran
        System.out.println("Luas Lingkaran: " + lingkaran.hitungLuas());
    }
}


```
- output
![Dokumentasi](Dokumentasi/O.png)

> Penjelasan kode

Dengan menerapkan OCP, kita dapat menambahkan jenis bentuk baru yang mengimplementasikan interface "Bentuk" tanpa harus mengubah kelas yang sudah ada. Misalnya, jika kita ingin menambahkan kelas "Segitiga" yang mengimplementasikan "Bentuk", **kita dapat melakukannya tanpa memodifikasi kode yang ada dalam kelas**  **"PersegiPanjang"** atau **"Lingkaran"**.


- Interface "Bentuk" (Shape): Merupakan kontrak yang menentukan bahwa setiap kelas yang mengimplementasikannya harus memiliki metode hitungLuas(). **Kode ini menerapkan OCP karena memungkinkan ekstensi dengan menambahkan kelas bentuk baru yang mengimplementasikan interface ini tanpa mengubah kode yang sudah ada**.

```
interface Bentuk {
    double hitungLuas();
}
```



- Kelas "PersegiPanjang" (Rectangle): Implementasi konkrit dari interface "Bentuk" untuk menghitung luas persegi panjang. **Kode ini menerapkan OCP karena mengimplementasikan kontrak interface "Bentuk" tanpa mempengaruhi kelas lain dan dapat digunakan bersama dengan kode yang sudah ada.**

```
class PersegiPanjang implements Bentuk {
    private double panjang;
    private double lebar;
    
    // Konstruktor PersegiPanjang untuk menginisialisasi panjang dan lebar
    public PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }
    
    // Implementasi metode hitungLuas() untuk menghitung luas persegi panjang
    public double hitungLuas() {
        return panjang * lebar;
    }
}
```



- Kelas "Lingkaran" (Circle): Implementasi konkrit dari interface "Bentuk" untuk menghitung luas lingkaran. Kode ini juga menerapkan OCP karena mengimplementasikan kontrak interface "Bentuk" **tanpa mempengaruhi kelas lain dan dapat digunakan bersama dengan kode yang sudah ada.**


```
class Lingkaran implements Bentuk {
    private double jariJari;
    
    // Konstruktor Lingkaran untuk menginisialisasi jari-jari
    public Lingkaran(double jariJari) {
        this.jariJari = jariJari;
    }
    
    // Implementasi metode hitungLuas() untuk menghitung luas lingkaran
    public double hitungLuas() {
        return Math.PI * jariJari * jariJari;
    }
}
```


- Kelas "Main": Menjalankan program utama. Meskipun tidak secara langsung terkait dengan OCP, kelas ini bertindak sebagai entry point untuk menjalankan logika utama.

```
public class Main {
    public static void main(String[] args) {
        // Membuat objek persegiPanjang dengan panjang 5 dan lebar 3
        Bentuk persegiPanjang = new PersegiPanjang(5, 3);
        
        // Membuat objek lingkaran dengan jari-jari 2.5
        Bentuk lingkaran = new Lingkaran(2.5);
        
        // Menampilkan luas persegi panjang
        System.out.println("Luas Persegi Panjang: " + persegiPanjang.hitungLuas());
        
        // Menampilkan luas lingkaran
        System.out.println("Luas Lingkaran: " + lingkaran.hitungLuas());
    }
}
```


> **Penjelasan Dari Analogi Yang Dibuat**

1. Interface `Bentuk`  di analogikan sebagai kontrak yang harus diikuti oleh setiap bentuk geometri. **Setiap bentuk geometri harus dapat menghitung luasnya**.

2. Kelas PersegiPanjang di analogikan sebagai sebuah persegi panjang dengan panjang dan lebar tertentu. Persegi panjang memiliki **kemampuan untuk menghitung luasnya**.

3. Kelas Lingkaran di analogikan sebagai sebuah lingkaran dengan jari-jari tertentu. L**ingkaran memiliki kemampuan untuk menghitung luasnya**.

4.  `hitungLuas() `pada kelas PersegiPanjang dan Lingkaran merupakan implementasi dari kontrak yang diberikan oleh interface `Bentuk`. Metode ini digunakan untuk menghitung luas masing-masing bentuk.

5. Pada `main()`, objek persegiPanjang diinisialisasi sebagai persegi panjang dengan **panjang 5** dan **lebar 3**.

6. Objek lingkaran diinisialisasi dengan jari-jari 2.5.

7. Pada `hitungLuas()`di persegiPanjang.
persegi panjang menghitung luasnya berdasarkan panjang dan lebar yang dimiliki.

8. Pada  lingkaran.`hitungLuas()`, lingkaran menghitung luasnya berdasarkan jari-jari yang dimiliki.


# 3.Liskov Substitution Principle (Prinsip Substitusi Liskov):

Prinsip ini menyatakan bahwa objek dari suatu kelas seharusnya dapat diganti dengan objek kelas turunan tanpa mengganggu integritas program. Dalam konteks ini, "diganti" adalah bahwa objek kelas turunan harus bisa menggantikan objek kelas induk dalam semua situasi tanpa menghasilkan hasil yang salah.

- Contoh analogi keluarga hewan dan kemampuan gerak mereka.


- Contoh program

```
class Burung {
    public void terbang() {
        System.out.println("Burung sedang terbang...");
    }
}
// Kelas Bebek (Turunan dari kelas Burung)
class Bebek extends Burung {
    public void berenang() {
        System.out.println("Bebek sedang berenang...");
    }
}
// Kelas BurungUnta (Turunan dari kelas Burung)
class BurungUnta extends Burung {
    public void berlari() {
        System.out.println("Burung unta sedang berlari...");
    }
}

public class Main {
    public static void main(String[] args) {
        // Polimorfisme: Objek Bebek merupakan turunan dari Burung
        Burung burung1 = new Bebek();
        
        // Polimorfisme: Objek BurungUnta merupakan turunan dari Burung
        Burung burung2 = new BurungUnta();
        
        burung1.terbang();
        burung2.terbang();
    }
}

```
- output
![Dokumentasi](Dokumentasi/L.png)

> **Penjelasan kode yang menerapkan prinsip Liskov Substitution Principle (LSP):**

Dengan menerapkan LSP, kita dapat menggunakan objek turunan dengan menggantikan objek induknya tanpa mengganggu perilaku yang diturunkan.

- Objek "burung1" diinisialisasi sebagai objek "Bebek". Menggunakan polimorfisme, objek "burung1" dapat diperlakukan sebagai "Burung" karena "Bebek" adalah turunannya. Ini menunjukkan LSP karena objek turunan dapat menggantikan objek induknya tanpa mempengaruhi fungsionalitas yang diharapkan (memanggil metode terbang()).

```
// Polimorfisme: Objek Bebek merupakan turunan dari Burung
        Burung burung1 = new Bebek();
```


Method:

`burung1.terbang();`


- Objek "burung2" diinisialisasi sebagai objek "BurungUnta". Menggunakan polimorfisme, objek "burung2" juga dapat diperlakukan sebagai "Burung" karena "BurungUnta" adalah turunannya.

```
// Polimorfisme: Objek BurungUnta merupakan turunan dari Burung
        Burung burung2 = new BurungUnta();
```
Method:
        
`burung2.terbang();`


> Penjelasan Analogi Yang Dibuat

1. Kelas Burung dapat di analogikan sebagai kelas induk yang merepresentasikan keluarga burung. Kelas ini memiliki kemampuan terbang.

2. Kelas Bebek dapat di analogikan sebagai spesies bebek yang merupakan bagian dari keluarga burung. Bebek memiliki kemampuan berenang, selain kemampuan terbang yang diwarisi dari keluarga burung.

3. Kelas BurungUnta dapat di analogikan sebagai spesies burung unta yang juga merupakan bagian dari keluarga burung. Burung unta memiliki kemampuan berlari, selain kemampuan terbang yang diwarisi dari keluarga burung.

4. Pada bagian `main()`, objek burung1 diinisialisasi sebagai seekor bebek, dan objek burung2 diinisialisasi sebagai seekor burung unta.

5. Pada pemanggilan burung1.`terbang()`, meskipun objek burung1 merupakan bebek, tetapi karena bebek adalah bagian dari keluarga burung yang tidak memiliki kemampuan terbang, maka outputnya adalah "Burung sedang terbang...".

6. Pada pemanggilan burung2.`terbang()`, objek burung2 merupakan burung unta yang juga merupakan bagian dari keluarga burung tetapi tidak bisa terbang, sehingga outputnya adalah "Burung sedang terbang...".



# 4.Interface Segregation Principle (Prinsip Pemisahan Antarmuka):

Prinsip ini menyatakan bahwa klien (client) tidak harus dipaksa untuk bergantung pada antarmuka yang tidak mereka gunakan. Idealnya, antarmuka harus dibagi menjadi bagian-bagian yang lebih kecil dan spesifik.

- Contoh analogi perangkat pencetak (printer) dan pemindai (scanner) dalam sebuah ruangan kerja.



> Contoh kode

```
//antarmuka "Printer" yang mendefinisikan metode cetak().
interface Printer {
    void cetak();
}
//antarmuka "Scanner" yang mendefinisikan metode scan().
interface Scanner {
    void scan();
}

class PrinterSemuaDalamSatu implements Printer, Scanner {
    public void cetak() {
        System.out.println("Mencetak...");
    }
    
    public void scan() {
        System.out.println("Meng-scan...");
    }
}

class PrinterBiasa implements Printer {
    public void cetak() {
        System.out.println("Mencetak...");
    }
}

public class Main {
    public static void main(String[] args) {
        // Polimorfisme: Objek PrinterSemuaDalamSatu mengimplementasikan kedua antarmuka (Printer dan Scanner)
        Printer printer1 = new PrinterSemuaDalamSatu();
        
        // Polimorfisme: Objek PrinterBiasa hanya mengimplementasikan antarmuka Printer
        Printer printer2 = new PrinterBiasa();
        
        printer1.cetak();
        printer2.cetak();
        
        if (printer1 instanceof Scanner) {
            // Menggunakan operator instanceof untuk memeriksa apakah objek juga mengimplementasikan antarmuka Scanner
            // Casting objek printer1 ke Scanner untuk memanggil metode scan()
            ((Scanner) printer1).scan();
        }
    }
}

```
- output
![Dokumentasi](Dokumentasi/I.png)

> Penjelasan kode yang menerapkan prinsip Interface Segregation Principle (ISP):


Dengan menerapkan ISP, kita memisahkan antarmuka berdasarkan fungsi yang terkait. Ini memungkinkan kelas yang mengimplementasikan antarmuka hanya perlu mengimplementasikan metode yang relevan dengan fungsionalitas yang diperlukan, menghindari adanya metode yang tidak relevan dan memastikan adanya kohesi antara antarmuka dan implementasinya.


1. Antarmuka "Printer": Mendefinisikan metode cetak().
```
interface Printer {
    void cetak();
}
```

2. Antarmuka "Scanner": Mendefinisikan metode scan().
```
interface Scanner {
    void scan();
}
```


3. Kelas "PrinterSemuaDalamSatu": Mengimplementasikan kedua antarmuka (Printer dan Scanner) dengan mengimplementasikan metode cetak() dan scan().
**Kode ini menerapkan ISP karena memisahkan antarmuka berdasarkan fungsi (cetak dan scan), sehingga kelas yang mengimplementasikan hanya perlu mengimplementasikan metode yang relevan**.

```
class PrinterSemuaDalamSatu implements Printer, Scanner {
    public void cetak() {
        System.out.println("Mencetak...");
    }
    
    public void scan() {
        System.out.println("Meng-scan...");
    }
}
```


4. Kelas "PrinterBiasa": Mengimplementasikan antarmuka "Printer" dengan mengimplementasikan metode cetak().

```
class PrinterBiasa implements Printer {
    public void cetak() {
        System.out.println("Mencetak...");
    }
}
```


5. Kelas "Main": Menjalankan program utama. Meskipun tidak secara langsung terkait dengan ISP, kelas ini bertindak sebagai entry point untuk menjalankan logika utama.


```
public class Main {
    public static void main(String[] args) {
        // Polimorfisme: Objek PrinterSemuaDalamSatu mengimplementasikan kedua antarmuka (Printer dan Scanner)
        Printer printer1 = new PrinterSemuaDalamSatu();
        
        // Polimorfisme: Objek PrinterBiasa hanya mengimplementasikan antarmuka Printer
        Printer printer2 = new PrinterBiasa();
        
        printer1.cetak();
        printer2.cetak();
        
        if (printer1 instanceof Scanner) {
            // Menggunakan operator instanceof untuk memeriksa apakah objek juga mengimplementasikan antarmuka Scanner
            // Casting objek printer1 ke Scanner untuk memanggil metode scan()
            ((Scanner) printer1).scan();
        }
    }
}
```



> **Penjelasan Dari Analogi Yang Dibuat**

1. Interface Printer dapat di analogikan sebagai sebuah perangkat pencetak yang memiliki kemampuan mencetak dokumen.

2. Interface Scanner dapat di analogikan sebagai sebuah perangkat pemindai yang memiliki kemampuan memindai dokumen.

3. Kelas PrinterSemuaDalamSatu dapat di analogikan sebagai sebuah perangkat multifungsi yang memiliki kemampuan mencetak dan memindai. Perangkat ini dapat melakukan kedua fungsi tersebut.

4. Kelas PrinterBiasa dapat di analogikan sebagai sebuah perangkat pencetak biasa yang hanya memiliki kemampuan mencetak. Perangkat ini tidak memiliki kemampuan pemindai.

5. Pada bagian `main()`, objek printer1 diinisialisasi sebagai perangkat PrinterSemuaDalamSatu, yang dapat mencetak dan memindai. Objek printer2 diinisialisasi sebagai perangkat PrinterBiasa, yang hanya dapat mencetak.

6. Pada pemanggilan printer1.`cetak()`, perangkat printer1 dapat melakukan fungsi mencetak, sehingga outputnya adalah "Mencetak...".

7. Pada pemanggilan printer2.`cetak()`, perangkat printer2 juga dapat melakukan fungsi mencetak, sehingga outputnya adalah "Mencetak...".

Karena objek printer1 merupakan perangkat PrinterSemuaDalamSatu yang juga mengimplementasikan interface Scanner, kita menggunakan operator `instanceof` untuk memeriksa apakah objek tersebut juga merupakan pemindai. Jika benar, maka objek tersebut di-cast menjadi Scanner dan dapat melakukan fungsi pemindaian. Namun, dalam contoh ini, objek printer1 tidak di-cast menjadi Scanner, sehingga pemindaian tidak dilakukan.


# 5.Dependency Inversion Principle (Prinsip Inversi Ketergantungan):

Prinsip ini menyatakan bahwa modul tingkat tinggi tidak boleh bergantung pada modul tingkat rendah. Keduanya seharusnya bergantung pada abstraksi. Selain itu, abstraksi tidak boleh bergantung pada detail, tetapi detail seharusnya bergantung pada abstraksi. Dalam konteks ini, "tingkat tinggi" mengacu pada modul yang mengatur alur utama aplikasi, sedangkan "tingkat rendah" mengacu pada modul yang mengerjakan tugas-tugas spesifik.

- Contoh analogi dengan sebuah restoran yang memiliki dua metode pengiriman makanan: pengiriman melalui kurir dan pengiriman melalui layanan pesan antar.


- Contoh kode
```

// Interface yang mendefinisikan kontrak untuk layanan pengiriman
interface Pengiriman {
    void kirimPaket();
}

// Implementasi kelas layanan pengiriman menggunakan metode email
class PengirimanEmail implements Pengiriman {
    public void kirimPaket() {
        System.out.println("Mengirim paket melalui email...");
        // Logika pengiriman paket menggunakan email
    }
}

// Implementasi kelas layanan pengiriman menggunakan metode SMS
class PengirimanSMS implements Pengiriman {
    public void kirimPaket() {
        System.out.println("Mengirim paket melalui SMS...");
        // Logika pengiriman paket menggunakan SMS
    }
}

// Kelas pengguna layanan pengiriman
class PenggunaPengiriman {
    private Pengiriman pengiriman;
    
    // Konstruktor menerima instance Pengiriman sebagai dependensi
    public PenggunaPengiriman(Pengiriman pengiriman) {
        this.pengiriman = pengiriman;
    }
    
    // Metode penggunaan layanan pengiriman
    public void prosesPengiriman() {
        // Melakukan proses pengiriman menggunakan dependensi yang diberikan
        pengiriman.kirimPaket();
    }
}

public class Main {
    public static void main(String[] args) {
        // Membuat instance PengirimanEmail
        PengirimanEmail pengirimanEmail = new PengirimanEmail();
        
        // Membuat instance PenggunaPengiriman dengan dependensi PengirimanEmail
        PenggunaPengiriman penggunaPengiriman1 = new PenggunaPengiriman(pengirimanEmail);
        penggunaPengiriman1.prosesPengiriman();
        
        // Membuat instance PengirimanSMS
        PengirimanSMS pengirimanSMS = new PengirimanSMS();
        
        // Membuat instance PenggunaPengiriman dengan dependensi PengirimanSMS
        PenggunaPengiriman penggunaPengiriman2 = new PenggunaPengiriman(pengirimanSMS);
        penggunaPengiriman2.prosesPengiriman();
    }
}
```
- output
![Dokumentasi](Dokumentasi/D.png)

> **Penjelasan Code implementasi Prinsip Inversi Ketergantungan:**

1. Antarmuka Pengiriman:

```
interface Pengiriman {
    void kirimPaket();
}
```
Antarmuka ini adalah abstraksi yang diperlukan untuk layanan pengiriman. Ini merupakan contoh penggunaan abstraksi sebagai kontrak untuk layanan yang akan digunakan.

2. Kelas PenggunaPengiriman:
```
class PenggunaPengiriman {
    private Pengiriman pengiriman;
    
    public PenggunaPengiriman(Pengiriman pengiriman) {
        this.pengiriman = pengiriman;
    }
    
    public void prosesPengiriman() {
        pengiriman.kirimPaket();
    }
}
```
Penerapan Prinsip Inversi Ketergantungan ini memungkinkan fleksibilitas dan menghindari ketergantungan langsung pada implementasi konkret, sehingga mempermudah perluasan dan penggantian implementasi tanpa mengganggu kelas-kelas yang bergantung pada abstraksi tersebut.

Kelas `PenggunaPengiriman` menerima dependensi `Pengiriman` melalui konstruktor dan menggunakannya melalui metode `prosesPengiriman()`. Kelas ini bergantung pada abstraksi `Pengiriman` daripada implementasi konkret. Dengan instance `PenggunaPengiriman` dibuat, kita dapat memberikan implementasi apa pun dari `Pengiriman` misalnya `PengirimanEmail`, `PengirimanSMS` sesuai kebutuhan tanpa mengubah kode di kelas `PenggunaPengiriman`.

> **Penjelasan Dari Analogi Yang Dibuat**

1. Interface Pengiriman dapat dianalogikan sebagai kontrak yang diberikan oleh restoran kepada penyedia layanan pengiriman. Restoran hanya menentukan metode yang harus ada, yaitu `kirimPaket()`, tanpa peduli bagaimana implementasinya.

2. Implementasi PengirimanEmail dapat dianalogikan sebagai sebuah perusahaan pengiriman makanan yang menggunakan metode pengiriman melalui kurir. Mereka memiliki sistem pengiriman paket melalui email yang unik. Restoran bergantung pada layanan mereka dan hanya memanggil metode `kirimPaket()` untuk mengirim makanan.

3. Implementasi PengirimanSMS dapat dianalogikan sebagai perusahaan pengiriman makanan lain yang menggunakan metode pengiriman melalui layanan pesan antar. Mereka memiliki sistem pengiriman paket melalui SMS yang berbeda. Restoran juga bergantung pada layanan mereka dan menggunakan metode `kirimPaket()` untuk mengirim makanan.

4. Kelas PenggunaPengiriman dapat dianalogikan sebagai restoran itu sendiri. Mereka memiliki fungsionalitas untuk mengirim makanan kepada pelanggan, tetapi mereka tidak perlu tahu bagaimana metode pengiriman dilakukan. Mereka hanya membutuhkan sebuah objek yang mengimplementasikan interface Pengiriman sebagai dependensi.
