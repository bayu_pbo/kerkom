# 1. Contoh Kode SOLID (Single Responsibility Principle (SRP), Open-Closed Principle (OCP), Liskov Substitution Principle (LSP), Interface Segregation Principle (ISP), dan Dependency Inversion Principle (DIP)

**Single Responsibility Principle (SRP)**

> Food merupakan kelas yang merepresentasikan makanan. Memiliki atribut `name` (nama makanan) dan `price` (harga makanan).

```

class Food {
    private String name;
    private double price;
    
    public Food(String name, double price) {
        this.name = name;
        this.price = price;
    }
    
    public String getName() {
        return name;
    }
    
    public double getPrice() {
        return price;
    }
}
```
> Selanjutnya `FoodLogger` adalah kelas yang bertanggung jawab untuk melakukan `logging` terkait makanan. Memiliki metode `logFood()` yang menerima objek Food sebagai parameter dan mencetak pesan log terkait makanan tersebut.

```
class FoodLogger {
    public void logFood(Food food) {
        // Implementasi untuk melakukan logging terkait makanan
        String logMessage = "Makanan: " + food.getName() + ", Harga: " + food.getPrice();
        // Lakukan logging ke file atau sistem lainnya
        System.out.println(logMessage);
    }
}
```
> FoodOrder merupakan kelas yang digunakan untuk melakukan pemesanan makanan. Menerima objek Food saat diinisialisasi dan memiliki metode` placeOrder()` yang mencetak pesan konfirmasi pemesanan.

```
class FoodOrder {
    private Food food;
    
    public FoodOrder(Food food) {
        this.food = food;
    }
    
    public void placeOrder() {
        // Implementasi untuk melakukan pemesanan makanan
        // Proses pemesanan makanan
        System.out.println("Pesanan Anda untuk " + food.getName() + " telah diterima.");
    }
}
```


**Open-Closed Principle (OCP)**

> FoodProvider adalah sebuah interface yang mendefinisikan metode getFood(), yang harus diimplementasikan oleh kelas yang ingin menjadi penyedia makanan.

```
interface FoodProvider {
    public Food getFood();
}
```
> LocalFoodProvider dan InternationalFoodProvider adalah implementasi dari FoodProvider yang menghasilkan makanan lokal dan internasional.

```
class LocalFoodProvider implements FoodProvider {
    public Food getFood() {
        // Implementasi untuk mendapatkan makanan lokal
        return new Food("Nasi Goreng", 15000);
    }
}

class InternationalFoodProvider implements FoodProvider {
    public Food getFood() {
        // Implementasi untuk mendapatkan makanan internasional
        return new Food("Pizza", 20000);
    }
}
```


**` Liskov Substitution Principle (LSP)`**

> FoodDelivery adalah kelas yang bertanggung jawab untuk mengirimkan makanan kepada pelanggan. Memiliki metode deliver() yang menerima objek Food dan mencetak pesan pengiriman.


```
class FoodDelivery {
    public void deliver(Food food) {
        // Implementasi untuk mengirimkan makanan ke pelanggan
        System.out.println("Makanan " + food.getName() + " telah dikirim.");
    }
}
```
> FrozenFoodDelivery merupakan turunan dari FoodDelivery yang khusus digunakan untuk mengirim makanan beku. Di dalamnya terdapat metode deliver() yang melakukan pengecekan apakah makanan merupakan makanan beku atau tidak sebelum melakukan pengiriman.

```
class FrozenFoodDelivery extends FoodDelivery {
    public void deliver(Food food) {
        // Implementasi khusus untuk mengirim makanan beku
        System.out.println("Makanan beku " + food.getName() + " telah dikirim.");
    }
}
```


**Interface Segregation Principle (ISP)**

> FoodPreparer dan FoodCooker adalah interface yang menggambarkan perilaku persiapan dan memasak makanan

```
interface FoodPreparer {
    public void prepareFood();
}

interface FoodCooker {
    public void cookFood();
}
```
> LocalFoodCooker adalah implementasi dari FoodPreparer dan FoodCooker yang digunakan untuk persiapan dan memasak makanan lokal.

```
class LocalFoodCooker implements FoodPreparer, FoodCooker {
    public void prepareFood() {
        // Implementasi untuk persiapan makanan lokal
        System.out.println("Persiapan bahan makanan lokal...");
    }
    
    public void cookFood() {
        // Implementasi untuk memasak makanan lokal
        System.out.println("Memasak makanan lokal...");
    }
}
```
> InternationalFoodCooker adalah implementasi dari FoodCooker yang digunakan untuk memasak makanan internasional.

```
class InternationalFoodCooker implements FoodCooker {
    public void cookFood() {
        // Implementasi untuk memasak makanan internasional
        System.out.println("Memasak makanan internasional...");
    }
}
```


**Dependency Inversion Principle (DIP)**

> FoodRepository merupakan kelas yang bertanggung jawab untuk mendapatkan makanan dari FoodProvider. Di dalamnya terdapat metode getFood() yang mengambil makanan dari FoodProvider yang diberikan saat diinisialisasi.


```
class FoodRepository {
    private FoodProvider foodProvider;
    
    public FoodRepository(FoodProvider foodProvider) {
        this.foodProvider = foodProvider;
    }
    
    public Food getFood() {
        return foodProvider.getFood();
    }
}
```
> Di dalam fungsi `main()`, dilakukan contoh penggunaan kode. Pertama, LocalFoodProvider dipilih sebagai FoodProvider dan digunakan untuk mendapatkan makanan melalui FoodRepository. Selanjutnya, makanan tersebut dilogging menggunakan FoodLogger, dipesan menggunakan FoodOrder, dikirimkan menggunakan FoodDelivery, dipersiapkan menggunakan FoodPreparer, dan dimasak menggunakan FoodCooker.

```
public class Main {
    public static void main(String[] args) {
        // Contoh penggunaan kode
        FoodProvider foodProvider = new LocalFoodProvider();
        FoodRepository foodRepository = new FoodRepository(foodProvider);
        Food food = foodRepository.getFood();
        
        FoodLogger foodLogger = new FoodLogger();
        foodLogger.logFood(food);
        
        FoodOrder foodOrder = new FoodOrder(food);
        foodOrder.placeOrder();
        
        FoodDelivery foodDelivery = new FoodDelivery();
        foodDelivery.deliver(food);
        
        FoodPreparer foodPreparer = new LocalFoodCooker();
        foodPreparer.prepareFood();
        
        FoodCooker foodCooker = new LocalFoodCooker();
        foodCooker.cookFood();
    }
}
```
> Output Code:
![Dokumentasi](Dokumentasi/123.jpg)


- **Analogi Program:**


analogikan dengan sebuah restoran.

1. Food adalah menu makanan yang tersedia di restoran, dengan nama dan harga masing-masing makanan.

2. FoodLogger adalah catatan yang dibuat oleh restoran untuk mencatat informasi terkait makanan, seperti nama dan harga, yang berguna untuk keperluan pelaporan atau audit.

3. FoodOrder adalah proses pemesanan makanan oleh pelanggan di restoran. Ketika pelanggan memesan makanan, restoran mengonfirmasi pemesanan tersebut.

4. FoodProvider adalah penyedia makanan untuk restoran. Ada penyedia lokal dan penyedia internasional yang menyediakan makanan sesuai dengan permintaan restoran.

5. FoodDelivery adalah tim pengantaran restoran yang bertugas mengirimkan makanan pesanan pelanggan.

6. FrozenFoodDelivery adalah tim pengantaran restoran yang khusus mengirimkan makanan beku. Mereka memeriksa apakah makanan yang akan dikirimkan merupakan makanan beku sebelum melakukan pengiriman.

7. FoodPreparer dan FoodCooker adalah bagian dari tim dapur restoran yang bertanggung jawab untuk persiapan dan memasak makanan. Ada tim yang khusus menangani persiapan dan memasak makanan lokal, dan ada juga tim yang fokus pada memasak makanan internasional.

8. FoodRepository adalah tempat restoran mengambil makanan dari penyedia makanan. Restoran dapat mengambil makanan sesuai kebutuhan dari penyedia yang mereka pilih.

Di analogi ini, prinsip SOLID diterapkan untuk memisahkan tanggung jawab yang berbeda, seperti **pencatatan makanan**, **pemesanan makanan**, **pengiriman makanan**, **persiapan dan memasak makanan**, serta **mengambil makanan dari penyedia.** Hal ini memungkinkan restoran untuk memiliki komponen-komponen yang terpisah dan dapat digunakan secara fleksibel sesuai dengan kebutuhan mereka.

# 2. Contoh Solid dengan analogi motor

**Single Responsibility Principle (SRP)**: 

> Kelas Motor bertanggung jawab untuk menyimpan informasi tentang merek dan harga motor. Kelas MotorLogger bertanggung jawab untuk melakukan logging terkait motor. Kelas MotorShop bertanggung jawab untuk menjual motor.

```
// Single Responsibility Principle (SRP)
class Motor {
    private String brand;
    private int price;

    public Motor(String brand, int price) {
        this.brand = brand;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }
}

class MotorLogger {
    public void logMotor(Motor motor) {
        String logMessage = "Motor: " + motor.getBrand() + ", Harga: " + motor.getPrice();
        System.out.println(logMessage);
    }
}

class MotorShop {
    private Motor motor;

    public MotorShop(Motor motor) {
        this.motor = motor;
    }


    public void sellMotor() {
        System.out.println("Motor " + motor.getBrand() + " telah terjual.");
    }
}
```

`Open-Closed Principle (OCP)`: 

> Interface MotorProvider menyediakan metode `getMotor()` yang digunakan untuk mendapatkan motor. Kelas YamahaProvider dan HondaProvider mengimplementasikan interface ini dengan mengembalikan motor Yamaha dan Honda secara spesifik.

```
// Open-Closed Principle (OCP)
interface MotorProvider {
    Motor getMotor();
}

class YamahaProvider implements MotorProvider {
    public Motor getMotor() {
        return new Motor("Yamaha", 20000000);
    }
}

class HondaProvider implements MotorProvider {
    public Motor getMotor() {
        return new Motor("Honda", 18000000);
    }
}
```
**Liskov Substitution Principle (LSP):**

> Kelas MotorWorkshop memiliki metode repairMotor() yang digunakan untuk memperbaiki motor secara umum. Kelas YamahaWorkshop merupakan turunan dari MotorWorkshop dan mengimplementasikan metode repairMotor() dengan implementasi spesifik untuk memperbaiki motor Yamaha.


```
// Liskov Substitution Principle (LSP)
class MotorWorkshop {
    public void repairMotor(Motor motor) {
        System.out.println("Motor " + motor.getBrand() + " sedang diperbaiki.");
    }
}

class YamahaWorkshop extends MotorWorkshop {
    public void repairMotor(Motor motor) {
        if (motor.getBrand().equals("Yamaha")) {
            System.out.println("Motor Yamaha sedang diperbaiki.");
        } else {
            throw new IllegalArgumentException("Motor yang diperbaiki harus motor Yamaha.");
        }
    }
}
```
**Interface Segregation Principle (ISP):**

> Interface MotorMaintenance menyediakan metode `performMaintenance()` yang digunakan untuk melakukan perawatan motor. Interface MotorWash menyediakan metode `washMotor()` yang digunakan untuk mencuci motor. Kelas BasicMotorMaintenance dan PremiumMotorMaintenance mengimplementasikan interface-interface tersebut dengan implementasi perawatan dasar dan perawatan premium serta mencuci motor.


```
// Interface Segregation Principle (ISP)
interface MotorMaintenance {
    void performMaintenance();
}

interface MotorWash {
    void washMotor();
}

class BasicMotorMaintenance implements MotorMaintenance {
    public void performMaintenance() {
        System.out.println("Melakukan perawatan dasar motor...");
    }
}

class PremiumMotorMaintenance implements MotorMaintenance, MotorWash {
    public void performMaintenance() {
        System.out.println("Melakukan perawatan premium motor...");
    }

    public void washMotor() {
        System.out.println("Mencuci motor...");
    }
}
```
**Dependency Inversion Principle (DIP)**:

> Kelas MotorRepository menerapkan prinsip DIP dengan menggunakan MotorProvider sebagai parameter konstruktor. Hal ini memungkinkan kelas MotorRepository untuk bekerja dengan berbagai jenis MotorProvider, seperti YamahaProvider atau HondaProvider, tanpa harus bergantung pada implementasi khususnya.


```
// Dependency Inversion Principle (DIP)
class MotorRepository {
    private MotorProvider motorProvider;

    public MotorRepository(MotorProvider motorProvider) {
        this.motorProvider = motorProvider;
    }

    public Motor getMotor() {
        return motorProvider.getMotor();
    }
}
```
> Di dalam metode main(), kita melakukan penggunaan contoh dari kelas-kelas yang telah diimplementasikan. Misalnya, kita menggunakan YamahaProvider untuk mendapatkan motor Yamaha dari MotorRepository. Kemudian, kita menggunakan MotorLogger untuk melakukan logging motor, MotorShop untuk menjual motor, MotorWorkshop untuk memperbaiki motor, MotorMaintenance untuk melakukan perawatan, dan MotorWash untuk mencuci motor.

```
public class Main {
    public static void main(String[] args) {
        // Contoh penggunaan kelas-kelas di atas
        MotorProvider motorProvider = new YamahaProvider();
        MotorRepository motorRepository = new MotorRepository(motorProvider);
        Motor motor = motorRepository.getMotor();

        MotorLogger motorLogger = new MotorLogger();
        motorLogger.logMotor(motor);

        MotorShop motorShop = new MotorShop(motor);
        motorShop.sellMotor();

        MotorWorkshop motorWorkshop = new YamahaWorkshop();
        motorWorkshop.repairMotor(motor);

        MotorMaintenance motorMaintenance = new BasicMotorMaintenance();
        motorMaintenance.performMaintenance();

        MotorWash motorWash = new PremiumMotorMaintenance();
        motorWash.washMotor();
    }
}
```
> Dengan menerapkan prinsip-prinsip SOLID, kode menjadi lebih terstruktur, mudah dimengerti, dan lebih mudah untuk diperluas atau dimodifikasi di masa depan. Setiap kelas memiliki tanggung jawab yang jelas dan berkolaborasi dengan baik melalui interface yang sesuai.

> Output Code

![Dokumentasi](Dokumentasi/1234.jpg)


**Analogi dari kode diatas:**


- Single Responsibility Principle (SRP): 

Dalam bengkel motor, kita memiliki tiga komponen terkait motor. Pertama, kita memiliki motor itu sendiri yang memiliki merek dan harga. Kedua, kita memiliki logger yang bertanggung jawab untuk mencatat informasi terkait motor. Ketiga, kita memiliki toko motor yang menjual motor dan memberikan informasi tentang penjualan. Masing-masing komponen ini memiliki tanggung jawabnya sendiri, yaitu motor (merek dan harga), logger (logging informasi motor), dan toko motor (penjualan motor).

- Open-Closed Principle (OCP): 

Analoginya adalah bengkel motor memiliki dua penyedia motor, yaitu Yamaha dan Honda. Keduanya mengimplementasikan antarmuka MotorProvider yang memiliki metode getMotor() untuk mendapatkan motor. Dengan menggunakan prinsip OCP, jika ingin menambahkan penyedia motor baru, misalnya Suzuki, kita dapat membuat kelas baru yang mengimplementasikan MotorProvider tanpa mengubah kode yang ada.

- Liskov Substitution Principle (LSP): 

Dalam bengkel motor, kita memiliki bengkel motor umum dan bengkel motor Yamaha yang merupakan turunan dari bengkel motor umum. Bengkel motor umum dapat memperbaiki semua jenis motor, sedangkan bengkel motor Yamaha memiliki implementasi khusus untuk memperbaiki motor Yamaha. Jika ingin memperbaiki motor Yamaha, kita dapat menggunakan bengkel motor Yamaha tanpa mempengaruhi logika perbaikan di bengkel motor umum.

- Interface Segregation Principle (ISP):

 Dalam bengkel motor, kita memiliki dua jenis perawatan motor, yaitu perawatan dasar dan perawatan premium. Perawatan dasar hanya melibatkan proses perawatan dasar, sedangkan perawatan premium melibatkan proses perawatan dasar dan mencuci motor. Dengan menggunakan prinsip ISP, kita memisahkan antarmuka MotorMaintenance untuk perawatan dasar dan MotorWash untuk mencuci motor. Ini memungkinkan bengkel motor untuk menggunakan perawatan dasar atau perawatan premium sesuai dengan kebutuhan.

- Dependency Inversion Principle (DIP):

Dalam bengkel motor, kita memiliki MotorRepository yang bertanggung jawab untuk menyediakan motor. Namun, MotorRepository tidak bergantung pada implementasi langsung dari MotorProvider. Sebaliknya, MotorRepository menerima objek MotorProvider melalui konstruktor. Ini memungkinkan fleksibilitas dalam penggunaan berbagai penyedia motor tanpa mengubah kode MotorRepository.

Dengan menerapkan prinsip-prinsip SOLID dalam konteks bengkel motor, kita memisahkan tanggung jawab, menghindari ketergantungan yang tidak perlu, dan membuat sistem lebih fleksibel dan mudah dimengerti.

# 3.**Kelemahan dan Solusi SOLID patern**


1. Kehilangan fleksibilitas: 

Implementasi SOLID principles dapat membuat kode menjadi lebih kompleks dan terstruktur. Namun, terkadang prinsip-prinsip ini dapat membatasi fleksibilitas dalam mengubah kode atau menambahkan fitur baru. Misalnya, prinsip "Open/Closed Principle" (OCP) mendorong penggunaan polimorfisme untuk memperluas perilaku kelas tanpa mengubah kode yang ada. Namun, jika terdapat banyak implementasi yang bervariasi, penambahan fitur baru bisa menjadi rumit.

**Solusi**: Saat mengimplementasikan SOLID principles, perlu diperhatikan keseimbangan antara fleksibilitas dan terstruktur. Jika perubahan atau penambahan fitur yang sering terjadi, mungkin perlu mempertimbangkan trade-off antara SOLID principles dengan kebutuhan fleksibilitas yang lebih besar.

2. Kompleksitas yang meningkat: 

SOLID principles mendorong pembuatan struktur yang lebih terdefinisi dengan memisahkan tanggung jawab dan mengurangi ketergantungan antar kelas. Namun, hal ini dapat meningkatkan kompleksitas desain, terutama jika prinsip-prinsip ini diterapkan secara berlebihan atau tanpa mempertimbangkan konteks yang sesuai.

**Solusi**: Gunakan SOLID principles secara proporsional dan sesuai kebutuhan. Tidak semua prinsip harus diterapkan secara ketat dalam setiap kasus. Pahami konteks perangkat lunak yang sedang Anda bangun, serta tim dan sumber daya yang tersedia. Terapkan prinsip-prinsip ini dengan bijak dan sesuaikan dengan kompleksitas proyek yang sedang Anda kerjakan.

3. Over-engineering: 

Salah satu risiko dalam menerapkan SOLID principles adalah over-engineering, yaitu memperkenalkan kelebihan kompleksitas dan abstraksi yang tidak diperlukan. Misalnya, menggunakan pola desain yang rumit atau membagi kelas yang sebenarnya sederhana menjadi beberapa kelas yang kompleks.

**Solusi:** Ingatlah bahwa prinsip-prinsip SOLID hanyalah panduan, bukan aturan yang mutlak. Jika menerapkan SOLID principles mengarah pada over-engineering, evaluasi kembali desain Anda dan pertimbangkan apakah penggunaan prinsip-prinsip tersebut benar-benar memberikan manfaat yang signifikan pada proyek yang sedang Anda kerjakan.

4. Kurva pembelajaran: 

SOLID principles memerlukan pemahaman yang baik tentang konsep-konsep seperti kohesi, kopling, polimorfisme, dan sebagainya. Penerapannya yang benar membutuhkan waktu dan pengalaman. Ketika anggota tim yang belum terbiasa dengan SOLID principles bergabung dengan proyek, ada kemungkinan terjadi kurva pembelajaran yang curam.

**Solusi**: Dalam tim yang belum terbiasa dengan SOLID principles, berikan pelatihan dan bimbingan yang memadai. Sertakan praktik-praktik terbaik dan contoh implementasi SOLID principles dalam kode yang ada. Juga, pastikan untuk melakukan peer review dan kolaborasi aktif dalam tim untuk mempercepat pembelajaran dan meningkatkan pemahaman tentang SOLID principles.

